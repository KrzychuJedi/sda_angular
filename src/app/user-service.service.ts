import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserServiceService {

  constructor(private http:HttpClient) { }

  findUsers(): Observable<User[]> {
    return this.http.get<User[]>('http://localhost:8080/api/users')
  }

}

export interface User {
  firstName: string;
  lastName: string;
  email: string;
  id: string;
}


