import { Component, OnInit } from '@angular/core';
import {User, UserServiceService} from '../user-service.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  users$: Observable<User[]>;

  constructor(private userService: UserServiceService ) { }

  ngOnInit() {
    this.users$ = this.userService.findUsers();
  }

}
