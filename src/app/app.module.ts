import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserPageComponent } from './user-page/user-page.component';
import {RouterModule} from '@angular/router';
import { CoursePageComponent } from './course-page/course-page.component';
import {HttpClientModule} from '@angular/common/http';
import {UserServiceService} from './user-service.service';

@NgModule({
  declarations: [
    AppComponent,
    UserPageComponent,
    CoursePageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'users',
        component: UserPageComponent
      },{
        path: 'courses',
        component: CoursePageComponent
      }
    ]),
    HttpClientModule
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
